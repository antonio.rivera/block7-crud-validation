package com.example.block7crudvalidation.controller;


import com.example.block7crudvalidation.dto.StudentInputDto;
import com.example.block7crudvalidation.dto.StudentOutputDto;
import com.example.block7crudvalidation.dto.converter.StudentDtoConverter;
import com.example.block7crudvalidation.model.Asignatura;
import com.example.block7crudvalidation.model.Persona;
import com.example.block7crudvalidation.model.Student;
import com.example.block7crudvalidation.service.PersonaService;
import com.example.block7crudvalidation.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;
    private final PersonaService personaService;
    private final StudentDtoConverter studentDtoConverter;




    /**
     * Crear estudiante
     *
     * @param studentInputDto
     * @return Student
     */
    @PostMapping("/crearStudent")
    public ResponseEntity<Student> crearStudent(@RequestBody StudentInputDto studentInputDto){
        Student student = studentDtoConverter.converToInputStudent(studentInputDto);
        studentService.crearStudent(student);
        Persona persona = student.getPersona();
        persona.setStudent_id(student.getId_student());
        personaService.modificarPersona(persona);
        return ResponseEntity.ok(student);
    }



    /**
     * Mostrar todos los estudiantes
     *
     * @return List<Student>
     */
    @GetMapping("/mostrarStudents")
    public ResponseEntity<List<Student>> mostrarStudents(){
        return ResponseEntity.ok(studentService.mostrarStudents());
    }


    /**
     * Mostrar estudiante por id
     *
     * @param id_student
     * @param outputType
     * @return ?
     */
    @GetMapping("/{id_student}")
    public ResponseEntity<?> mostrarStudentPorId(@PathVariable Integer id_student, @RequestParam(value = "outputType",defaultValue = "simple") String outputType){
        if (outputType.equals("full")){
            StudentOutputDto studentOutputDto =
                    studentDtoConverter
                            .converToFullOutput(studentService.mostrarPorId(id_student));
            return ResponseEntity.ok(studentOutputDto);
        } else if (outputType.equals("simple")) {
            StudentInputDto studentInputDto =
                    studentDtoConverter
                            .converToSimpleOutput(studentService.mostrarPorId(id_student));
            return ResponseEntity.ok(studentInputDto);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    /**
     * Modificar estudiante
     * @param id_student
     * @param student
     * @return Student
     */

    @PutMapping("/modificarStudent/{id_student}")
    public ResponseEntity<Student> modificarStudent(@PathVariable Integer id_student, @RequestBody Student student){
        return ResponseEntity.ok(studentService.modificarStudent(id_student,student));
    }



    /**
     * Eliminar estudiante
     * @param id_student
     * @return Student
     */
    @DeleteMapping("/eliminarStudent/{id_student}")
    public ResponseEntity<Student> eliminarStudent(@PathVariable Integer id_student){
        return ResponseEntity.ok(studentService.eliminarStudent(id_student));
    }


    /**
     * Mostrar todas las asignaturas que tiene un estudiante
     * @param id
     * @return List<Asignatura>
     */
    @GetMapping("/estudiante-asignatura/{id}")
    public ResponseEntity<List<Asignatura>> estudianteAsignatura(@PathVariable Integer id){
        return ResponseEntity.ok(studentService.estudianteAsignaturas(id));
    }



    /**
     * Agregar asignaturas a un estudiante
     *
     * @param id
     * @param listaAsignaturas
     * @return Student
     */
    @PutMapping("/agregarAsignaturas/{id}")
    public ResponseEntity<Student> agregarAsignaturas(@PathVariable Integer id, @RequestBody List<Integer> listaAsignaturas ){
        return ResponseEntity.ok(studentService.agregarAsignaturas(listaAsignaturas,id));
    }

    /**
     * Eliminar las asignaturas de un estudiante
     * @param id
     * @param listaAsignaturas
     * @return Student
     */
    @PutMapping("/eliminarAsignaturas/{id}")
    public ResponseEntity<Student> eliminarAsignaturas(@PathVariable Integer id, @RequestBody List<Integer> listaAsignaturas){
        return ResponseEntity.ok(studentService.eliminarAsignaturas(listaAsignaturas, id));
    }


}
