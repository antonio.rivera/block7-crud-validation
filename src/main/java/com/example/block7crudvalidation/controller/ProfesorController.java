package com.example.block7crudvalidation.controller;



import com.example.block7crudvalidation.dto.ProfesorInputDto;
import com.example.block7crudvalidation.dto.converter.ProfesorDtoConverter;
import com.example.block7crudvalidation.model.Persona;
import com.example.block7crudvalidation.model.PersonaRepository;
import com.example.block7crudvalidation.model.Profesor;
import com.example.block7crudvalidation.service.ProfesorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/profesor")
@RequiredArgsConstructor
public class ProfesorController {

    private final ProfesorService profesorService;
    private final PersonaRepository personaRepository;
    private final ProfesorDtoConverter profesorDtoConverter;



    /**
     * Crear profesor
     *
     * @param profesorInputDto
     * @return Profesor
     */
    @PostMapping("/crearProfesor")
    public ResponseEntity<Profesor> crearProfesor(@RequestBody ProfesorInputDto profesorInputDto){
        Persona persona = personaRepository.findById(profesorInputDto.getId_persona()).orElse(null);
        Profesor profesor = profesorDtoConverter.convertInputToProfesor(profesorInputDto);
        if (persona.getStudent_id()==null){
            profesor.setPersona(persona);
            persona.setProfesor_id(profesorService.crearProfesor(profesor).getId_profesor());
            personaRepository.save(persona);
            return ResponseEntity.ok(profesor);
        }
        return null;
    }


    /**
     * Mostrar profesores
     *
     * @return List<Profesor>
     */
    @GetMapping("/mostrarProfesores")
    public ResponseEntity<List<Profesor>> mostrarProfesores(){
        return ResponseEntity.ok(profesorService.mostrarProfesores());
    }


    /**
     * Mostrar profesor por id
     *
     * @param id_profesor
     * @return ?
     */
    @GetMapping("/{id_Profesor}")
    public ResponseEntity<?> mostrarProfesorPorId(@PathVariable Integer id_profesor /*@RequestParam(value = "outputType",defaultValue = "simple") String outputType*/){
        /*if (outputType.equals("full")){
            ProfesorOutputDto ProfesorOutputDto =
                    ProfesorDtoConverter
                            .converToFullOutput(ProfesorService.mostrarPorId(id_Profesor));
            return ResponseEntity.ok(ProfesorOutputDto);
        } else if (outputType.equals("simple")) {
            ProfesorInputDto ProfesorInputDto =
                    ProfesorDtoConverter
                            .converToSimpleOutput(ProfesorService.mostrarPorId(id_Profesor));
            return ResponseEntity.ok(ProfesorInputDto);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();*/
        return ResponseEntity.ok(profesorService.mostrarPorId(id_profesor));

    }


    /**
     * Modificar profesor
     *
     * @param id_profesor
     * @param profesor
     * @return Profesor
     */
    @PutMapping("/modificarProfesor/{id_Profesor}")
    public ResponseEntity<Profesor> modificarProfesor(@PathVariable Integer id_profesor, @RequestBody Profesor profesor){
        return ResponseEntity.ok(profesorService.modificarProfesor(id_profesor,profesor));
    }


    /**
     * Eliminar profesor
     *
     * @param id_profesor
     * @return Profesor
     */
    @DeleteMapping("/eliminarProfesor/{id_Profesor}")
    public ResponseEntity<Profesor> eliminarProfesor(@PathVariable Integer id_profesor){
        return ResponseEntity.ok(profesorService.eliminarProfesor(id_profesor));
    }

}
