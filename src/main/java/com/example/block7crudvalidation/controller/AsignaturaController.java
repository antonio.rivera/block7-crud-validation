package com.example.block7crudvalidation.controller;

import com.example.block7crudvalidation.model.Asignatura;
import com.example.block7crudvalidation.service.AsignaturaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/asignaturas")
@RequiredArgsConstructor
public class AsignaturaController {
    private final AsignaturaService asignaturaService;

    /**
     * Mostrar todas las asignaturas
     *
     * @return List<Asignatura>
     */
    @GetMapping
    public ResponseEntity<List<Asignatura>> mostrarAsignaturas(){
        return ResponseEntity.ok(asignaturaService.mostrarAsignaturas());
    }


    /**
     * Mostrar por id una asignatura
     *
     * @param id
     * @return Asignatura
     */
    @GetMapping("/{id}")
    public ResponseEntity<Asignatura> mostrarPorId(@PathVariable Integer id){
        return ResponseEntity.ok(asignaturaService.mostrarPorId(id));
    }

    /**
     * Crear una asignatura
     *
     * @param asignatura
     * @return Asignatura
     */
    @PostMapping("/crearAsignatura")
    public ResponseEntity<Asignatura> crearAsignatura(@RequestBody Asignatura asignatura){
        return ResponseEntity.ok(asignaturaService.crearAsignatura(asignatura));
    }

    /**
     * Modificar asignatura
     * @param id_asignatura
     * @param asignatura
     * @return Asignatura
     */
    @PutMapping("/modificar/{id_asignatura}")
    public ResponseEntity<Asignatura> modificarAsignatura(@PathVariable Integer id_asignatura, @RequestBody Asignatura asignatura){
        return ResponseEntity.ok(asignaturaService.modificarAsignatura(id_asignatura,asignatura));
    }


    /**
     * Eliminar asignatura
     * @param id
     * @return Asignatura
     */
    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<Asignatura> eliminarAsignatura(@PathVariable Integer id){
        return ResponseEntity.ok(asignaturaService.eliminarAsignatura(id));
    }

}
