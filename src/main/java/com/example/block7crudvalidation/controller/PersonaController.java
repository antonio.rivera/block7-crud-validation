package com.example.block7crudvalidation.controller;

import com.example.block7crudvalidation.dto.PersonaDto;
import com.example.block7crudvalidation.dto.PersonaDtoFullOutput;
import com.example.block7crudvalidation.dto.PersonaDtoSimpleOutput;
import com.example.block7crudvalidation.dto.converter.PersonaDtoConverter;
import com.example.block7crudvalidation.model.Persona;
import com.example.block7crudvalidation.model.Profesor;
import com.example.block7crudvalidation.model.Student;
import com.example.block7crudvalidation.service.PersonaService;
import com.example.block7crudvalidation.service.ProfesorService;
import com.example.block7crudvalidation.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class PersonaController {

    private final PersonaService personaService;
    private final PersonaDtoConverter personaDtoConverter;
    private final StudentService studentService;
    private final ProfesorService profesorService;

    /**
     * Crea una persona
     *
     * @param persona
     * @return personaDto
     */
    @PostMapping("/persona/crearPersona")
    public ResponseEntity<PersonaDto> crearPersona(@RequestBody Persona persona) throws Exception {
        //personaService.crearPersona(persona);
        PersonaDto personaDto = personaDtoConverter
                .converToDto(personaService.crearPersona(persona));
        return ResponseEntity.ok(personaDto);
    }




    /**
     * Busca persona por id_persona
     *
     * @param id
     * @param outputType
     * @return personaDtoFullOutput o personaDtoSimpleOutput
     */
    @GetMapping("/persona/porId/{id}")
    public ResponseEntity<?> findPersonaById(@PathVariable Integer id, @RequestParam(value = "outputType", defaultValue = "simple") String outputType) {
        if (outputType.equals("full")) {
            // Tengo que localizar el profesor o estudiante primero
            Persona persona = personaService.findPersonaById(id);
            PersonaDtoFullOutput personaDtoFullOutput = new PersonaDtoFullOutput();
            //PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
            if (persona.getStudent_id() != null) {
                Student student = studentService.mostrarPorId(persona.getStudent_id());
                personaDtoFullOutput = personaDtoConverter.fullOutput(persona);
                personaDtoFullOutput.setStudent(student);
                return ResponseEntity.ok(personaDtoFullOutput);
            } else if (persona.getProfesor_id() != null) {
                Profesor profesor = profesorService.mostrarPorId(persona.getProfesor_id());
                personaDtoFullOutput = personaDtoConverter.fullOutput(persona);
                personaDtoFullOutput.setProfesor(profesor);
                return ResponseEntity.ok(personaDtoFullOutput);
            }
        }
        return ResponseEntity.ok(personaDtoConverter.simpleOutput(personaService.findPersonaById(id)));
    }

    /**
     * Busca persona por usuario
     *
     * @param usuario
     * @return personaDto
     */
    @GetMapping("/persona/porUsuario/{usuario}")
    public ResponseEntity<?> findByCampoUsuario(@PathVariable String usuario, @RequestParam(value = "outputType", defaultValue = "simple") String outputType){
        Persona persona = personaService.findByCampoUsuario(usuario);
        if (outputType.equals("full")) {
             //Tengo que localizar el profesor o estudiante primero

            PersonaDtoFullOutput personaDtoFullOutput = new PersonaDtoFullOutput();
            //PersonaDtoSimpleOutput personaDtoSimpleOutput = new PersonaDtoSimpleOutput();
            if (persona.getStudent_id()!=null){
                System.out.println("1");
                Student student = studentService.mostrarPorId(persona.getStudent_id());
                personaDtoFullOutput = personaDtoConverter.fullOutput(persona);
                personaDtoFullOutput.setStudent(student);
                return ResponseEntity.ok(personaDtoFullOutput);
            } else if (persona.getProfesor_id()!=null){
                System.out.println("2");
                Profesor profesor = profesorService.mostrarPorId(persona.getProfesor_id());
                personaDtoFullOutput = personaDtoConverter.fullOutput(persona);
                personaDtoFullOutput.setProfesor(profesor);
                return ResponseEntity.ok(personaDtoFullOutput);
            }
            //System.out.println("2");
        }
            //return personaService.findByCampoUsuario(user);
            //return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        //PersonaDtoFullOutput personaDtoFullOutput = personaDtoConverter.fullOutput(personaService.findByCampoUsuario(usuario));
        //return ResponseEntity.ok(personaDtoFullOutput);
        // return ResponseEntity.ok(personaService.findByCampoUsuario(usuario));
        return ResponseEntity.ok(personaDtoConverter.simpleOutput(persona));
    }


    /**
     * Muestra todas las personas
     * @param outputType
     * @return List<PersonaDtoFullOutput> o List<PersonaDtoSimpleOutput>
     */
    @GetMapping("/personas")
     public ResponseEntity<List<?>> mostrarPersonas(@RequestParam(value = "outputType", defaultValue = "simple") String outputType){
        List<Persona> personas = personaService.mostrarPersonas();
        if(outputType.equals("full")){
            List<PersonaDtoFullOutput> personaDtoFullOutputList = new ArrayList<>();
            for(Persona p:personas){
                if (p.getProfesor_id()!=null){
                    PersonaDtoFullOutput personaDtoFullOutput = personaDtoConverter.fullOutput(p);
                    personaDtoFullOutput.setProfesor(profesorService.mostrarPorId(p.getProfesor_id()));
                    personaDtoFullOutputList.add(personaDtoFullOutput);
                }
                if (p.getStudent_id()!=null){
                    PersonaDtoFullOutput personaDtoFullOutput = personaDtoConverter.fullOutput(p);
                    personaDtoFullOutput.setStudent(studentService.mostrarPorId(p.getStudent_id()));
                    personaDtoFullOutputList.add(personaDtoFullOutput);
                }
            }
            return ResponseEntity.ok(personaDtoFullOutputList);
        } else if (outputType.equals("simple")){
            List<PersonaDtoSimpleOutput> personaDtoSimpleOutputs = new ArrayList<>();
            for(Persona p:personas){
                personaDtoSimpleOutputs.add(personaDtoConverter.simpleOutput(p));
            }
            return ResponseEntity.ok(personaDtoSimpleOutputs);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
     }


     @DeleteMapping("/borrarPersona/{id}")
    public ResponseEntity<Persona> borrarPersona(@PathVariable Integer id){
        return ResponseEntity.ok(personaService.eliminarPersona(id));
     }

     //
    @GetMapping("/persona/profesor/{id}")
    public ResponseEntity<Profesor> getProfesorById(@PathVariable Integer id){
        List<Persona> personaList = personaService.mostrarPersonas();
        Profesor profesor = new Profesor();
        for (Persona p:personaList){
            if (p.getProfesor_id()==id){
                profesor = profesorService.mostrarPorId(p.getProfesor_id());
            }
        }
        //Profesor profesor = profesorService.mostrarPorId(personaService.findPersonaById(id).getProfesor_id());
        return ResponseEntity.ok(profesor);
    }

}
