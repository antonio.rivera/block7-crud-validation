package com.example.block7crudvalidation.dto.converter;

import com.example.block7crudvalidation.dto.PersonaDto;
import com.example.block7crudvalidation.dto.PersonaDtoFullOutput;
import com.example.block7crudvalidation.dto.PersonaDtoSimpleOutput;
import com.example.block7crudvalidation.model.Persona;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PersonaDtoConverter {
    private final ModelMapper modelMapper;

    public PersonaDto converToDto(Persona persona){
        return modelMapper.map(persona,PersonaDto.class);
    }

    public PersonaDtoSimpleOutput simpleOutput(Persona persona){
        return modelMapper.map(persona,PersonaDtoSimpleOutput.class);
    }

    public PersonaDtoFullOutput fullOutput(Persona persona){
        return modelMapper.map(persona,PersonaDtoFullOutput.class);
    }

}
