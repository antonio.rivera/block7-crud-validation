package com.example.block7crudvalidation.dto.converter;

import com.example.block7crudvalidation.dto.StudentInputDto;
import com.example.block7crudvalidation.dto.StudentOutputDto;
import com.example.block7crudvalidation.model.Persona;
import com.example.block7crudvalidation.model.PersonaRepository;
import com.example.block7crudvalidation.model.Student;
import com.example.block7crudvalidation.model.StudentRepository;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;


@Component
@RequiredArgsConstructor
@Builder
public class StudentDtoConverter {
    private final ModelMapper modelMapper;
    private final PersonaRepository personaRepository;
    private final StudentRepository studentRepository;

    public Student converToInputStudent(StudentInputDto studentInputDto){
        // A la hora de crear un estudiante compruebo que no es profesor
        Persona persona = personaRepository.findById(studentInputDto.getId_persona()).orElse(null);
        if (persona.getProfesor_id()==null){
            //persona.setIsStudent(true);
            //persona.setStudent_id(studentInputDto.getId_student());
            studentInputDto.setPersona(persona);
            //persona.setStudent(modelMapper.map(studentInputDto,Student.class));
            return modelMapper.map(studentInputDto,Student.class);
        }
        return null;

    }

    public StudentInputDto converToSimpleOutput(Student student){
        return modelMapper.map(student,StudentInputDto.class);
    }

    public StudentOutputDto converToFullOutput(Student student){
        return modelMapper.map(student,StudentOutputDto.class);
    }
}
