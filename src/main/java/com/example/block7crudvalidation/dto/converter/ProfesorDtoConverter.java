package com.example.block7crudvalidation.dto.converter;


import com.example.block7crudvalidation.dto.ProfesorInputDto;
import com.example.block7crudvalidation.dto.ProfesorOutputDto;
import com.example.block7crudvalidation.dto.StudentInputDto;
import com.example.block7crudvalidation.dto.StudentOutputDto;
import com.example.block7crudvalidation.model.Profesor;
import com.example.block7crudvalidation.model.Student;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProfesorDtoConverter {
    private final ModelMapper modelMapper;
    public Profesor convertInputToProfesor(ProfesorInputDto profesorInputDto){
        return modelMapper.map(profesorInputDto,Profesor.class);
    }
    public ProfesorInputDto converToSimpleOutput(Profesor profesor){
        return modelMapper.map(profesor,ProfesorInputDto.class);
    }

    public ProfesorOutputDto converToFullOutput(Profesor profesor){
        return modelMapper.map(profesor, ProfesorOutputDto.class);
    }
}
