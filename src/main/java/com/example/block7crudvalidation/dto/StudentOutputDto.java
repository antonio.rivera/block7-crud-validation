package com.example.block7crudvalidation.dto;

import com.example.block7crudvalidation.model.Persona;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentOutputDto {
    private Integer id_student;//  [pk, increment]
    private Integer num_hours_week;
    private String coments;
    private String branch;
    private Integer id_persona;
    private Persona persona;
}
