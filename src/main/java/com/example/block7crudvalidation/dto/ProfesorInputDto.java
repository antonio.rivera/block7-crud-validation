package com.example.block7crudvalidation.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class ProfesorInputDto {
    private Integer id_profesor;// [pk, increment]
    private String coments;//  string
    private String branch;
    private Integer id_persona;
}
