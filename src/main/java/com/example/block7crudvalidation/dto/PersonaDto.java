package com.example.block7crudvalidation.dto;

import lombok.*;

import java.util.Date;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class PersonaDto {
    //POner id
    private Integer id_persona;
    private String usuario;//  [not null max-length: 10 min-length: 6]
    private String password;// [not null]
    private String name;// [not null]
    private String surname;
    private String company_email;// [not null ]
    private String personal_email;// [not null]
    private String city;// [not null]
    private Boolean active;// [not null]
    private Date created_date;// [not null]
    private String imagen_url;
    private Date termination_date;
    private Integer student_id;


    private Integer profesor_id;
}
