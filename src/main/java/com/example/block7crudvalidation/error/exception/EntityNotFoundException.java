package com.example.block7crudvalidation.error.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;

//@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends ResponseStatusException {
    public EntityNotFoundException(String message){
        super(HttpStatus.NOT_FOUND,message);
    }
}
