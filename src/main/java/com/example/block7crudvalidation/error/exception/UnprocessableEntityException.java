package com.example.block7crudvalidation.error.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;

//@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UnprocessableEntityException extends ResponseStatusException {

    public UnprocessableEntityException(String message){
        super(HttpStatus.UNPROCESSABLE_ENTITY,message);
    }
}
