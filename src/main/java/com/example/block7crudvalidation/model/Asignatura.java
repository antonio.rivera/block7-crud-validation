package com.example.block7crudvalidation.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor @NoArgsConstructor
@Table(name = "asignatura")
public class Asignatura {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_asignatura;// [pk, increment]
    //private String id_student; string [ref: > student.id_student] -- Un estudiante puede tener N asignaturas
    private String asignatura;// string -- Nombre de asignatura. Ejemplo: HTML, Angular, SpringBoot...
    private String coments;// string
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate initial_date;// date [not null], -- Fecha en que estudiante empezó a estudiar la asignatura
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate finish_date;// date -- Fecha en que estudiante termina de estudiar la asignatura

    /*@ManyToMany(mappedBy = "asignaturas")
    private List<Student> students;*/
}
