package com.example.block7crudvalidation.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {
    //Optional<Persona> findById(String id_student);
    // Optional<Persona> findByUsuario(String usuario);
}
