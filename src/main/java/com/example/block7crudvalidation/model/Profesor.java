package com.example.block7crudvalidation.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor @NoArgsConstructor
@Table(name = "profesor")
public class Profesor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_profesor;// [pk, increment]
    //private String id_persona;//  string [ref:- persona.id_persona] -- Relación OneToOne con la tabla persona.
    private String coments;//  string
    private String branch;//  string [not null] -- Materia principal que imparte. Por ejemplo: Front

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_persona")
    private Persona persona;

    @OneToMany(mappedBy = "profesor", cascade = CascadeType.ALL)
    private List<Student> students;
}
