package com.example.block7crudvalidation.model;


import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.annotation.Reference;

import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor @NoArgsConstructor
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_student;//  [pk, increment]
    private Integer num_hours_week;
    private String coments;
    private String branch;
    //private Integer id_persona;



    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_persona")
    private Persona persona;

    @ManyToMany
    @JoinTable(name = "student_asignaturas",
            joinColumns = @JoinColumn(name = "estudiante_id"),
            inverseJoinColumns = @JoinColumn(name = "asignaturas_id"))
    private List<Asignatura> asignaturas;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_profesor")
    private Profesor profesor;


}
