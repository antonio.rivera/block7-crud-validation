package com.example.block7crudvalidation.service;

import com.example.block7crudvalidation.model.Asignatura;
import com.example.block7crudvalidation.model.Student;

import java.util.List;

public interface StudentService {

    // Crear
    Student crearStudent(Student student);

    // Mostrar todos
    List<Student> mostrarStudents();

    // Mostrar por id
    Student mostrarPorId(Integer id_student);

    // Modificar
    Student modificarStudent(Integer id_student, Student student);

    // Eliminar
    Student eliminarStudent(Integer id_student);

    // Asignaturas que tiene un estudiante
    List<Asignatura> estudianteAsignaturas(Integer id);

    // Le asigno asignaturas al estudiante
    Student agregarAsignaturas(List<Integer> listaAsignaturas, Integer id);

    // Eliminar asignaturas a un estudiante
    Student eliminarAsignaturas(List<Integer> listaAsignaturas, Integer id);

}
