package com.example.block7crudvalidation.service;

import com.example.block7crudvalidation.model.Profesor;

import java.util.List;

public interface ProfesorService {
    // Crear
    Profesor crearProfesor(Profesor profesor);

    // Mostrar todos
    List<Profesor> mostrarProfesores();

    // Mostrar por id
    Profesor mostrarPorId(Integer id_profesor);

    // Modificar
    Profesor modificarProfesor(Integer id_profesor, Profesor profesor);

    // Eliminar
    Profesor eliminarProfesor(Integer id_profesor);
}
