package com.example.block7crudvalidation.service;

import com.example.block7crudvalidation.dto.PersonaDto;
import com.example.block7crudvalidation.model.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonaService {

    Persona findPersonaById(Integer id_persona);

    Persona findByCampoUsuario(String user);

    List<Persona> mostrarPersonas();

    Persona crearPersona(Persona persona) throws Exception;

    Persona modificarPersona(Persona persona);

    Persona eliminarPersona(Integer id);

}
