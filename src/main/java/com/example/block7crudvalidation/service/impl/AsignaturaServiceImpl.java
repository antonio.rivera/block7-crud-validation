package com.example.block7crudvalidation.service.impl;

import com.example.block7crudvalidation.model.Asignatura;
import com.example.block7crudvalidation.model.AsignaturaRepository;
import com.example.block7crudvalidation.model.StudentRepository;
import com.example.block7crudvalidation.service.AsignaturaService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AsignaturaServiceImpl implements AsignaturaService {
    

    private final AsignaturaRepository asignaturaRepository;
    private final StudentRepository studentRepository;
    
    @Override
    public Asignatura crearAsignatura(Asignatura asignatura) {
        return asignaturaRepository.save(asignatura);
    }

    @Override
    public List<Asignatura> mostrarAsignaturas() {
        return asignaturaRepository.findAll();
    }

    @Override
    public Asignatura mostrarPorId(Integer id_asignatura) {
        return asignaturaRepository.findById(id_asignatura).orElse(null);
    }

    @Override
    public Asignatura modificarAsignatura(Integer id_asignatura, Asignatura asignatura) {
        Asignatura asignaturaModified = asignaturaRepository.findById(id_asignatura).orElse(null);
        if (asignaturaRepository.findById(id_asignatura).isPresent()){
            //asignaturaModified.setId_asignatura(asignatura.getId_asignatura());
            asignaturaModified.setAsignatura(asignatura.getAsignatura());
            asignaturaModified.setComents(asignatura.getComents());
            asignaturaModified.setInitial_date(asignatura.getInitial_date());
            asignaturaModified.setFinish_date(asignatura.getFinish_date());
            asignaturaRepository.save(asignaturaModified);
        }
        return asignaturaModified;
    }

    @Override
    public Asignatura eliminarAsignatura(Integer id_asignatura) {
        Asignatura asignaturaEliminado = asignaturaRepository.findById(id_asignatura).orElse(null);
        if (asignaturaRepository.findById(id_asignatura).isPresent()){

//            if(studentRepository.)
            asignaturaRepository.deleteById(id_asignatura);
        }
        return asignaturaEliminado;
    }
}
