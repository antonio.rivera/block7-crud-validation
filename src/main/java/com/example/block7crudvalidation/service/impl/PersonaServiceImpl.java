package com.example.block7crudvalidation.service.impl;

import com.example.block7crudvalidation.error.exception.EntityNotFoundException;
import com.example.block7crudvalidation.error.exception.UnprocessableEntityException;
import com.example.block7crudvalidation.model.Persona;
import com.example.block7crudvalidation.model.PersonaRepository;
import com.example.block7crudvalidation.service.PersonaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Objects;


@Service
@RequiredArgsConstructor
public class PersonaServiceImpl implements PersonaService {


    private final PersonaRepository personaRepository;


    @Override
    public Persona findPersonaById(Integer id_persona) {
        Persona persona =
                personaRepository
                        .findById(id_persona)
                        .orElseThrow(()->new EntityNotFoundException("Persona no encontrada"));
        return persona;
    }


    @Override
    public Persona findByCampoUsuario(String usuario) {
        /*List<Persona> listaDePersonas = personaRepository.findAll();
        Persona persona =
                listaDePersonas
                .stream()
                .filter(persona1 -> persona1.getUsuario().equals(user))
                .findFirst()// Me va a obtener el primero nada mas. Para que el campo sea único habrá que meter la restricción al crear un nuevo usuario
                .orElse(null);*/
        return personaRepository.findByUsuario(usuario).orElse(null);
    }


    @Override
    public List<Persona> mostrarPersonas() {
        List<Persona> personas = personaRepository.findAll();
        return personas;
    }



    @Override
    public Persona crearPersona(Persona persona) throws UnprocessableEntityException {
        // Compruebo que el nombre de usuario no existe antes

        // Compruebo que todos los campos tienen algo
        try {
            if (camposOk(persona)){
                // Aquí hay fallo porque he tenido que lanzar la excepcion en el metodo findByCampoUsuario
                /*if (Objects.isNull(findByCampoUsuario(persona.getUsuario()))){

                    throw new UnprocessableEntityException("El nombre de usuario ya existe");
                }*/

                personaRepository.save(persona);
                return persona;
            }
        } catch (UnprocessableEntityException e) {
            throw new UnprocessableEntityException(e.getMessage());
        }
        return null;
    }

    @Override
    public Persona modificarPersona(Persona persona) {
        Persona personaResult = personaRepository.findById(persona.getId_persona()).orElse(null);
        personaResult.setStudent_id(persona.getStudent_id());
        personaRepository.save(personaResult);
        return personaResult;
    }

    /**
     * Comprueba que la persona no tiene estudiante ni profesor asignado para eliminarla
     *
     * @param id
     * @return
     */
    @Override
    public Persona eliminarPersona(Integer id) {
        Persona personaResult = personaRepository.findById(id).orElse(null);
        if(personaResult.getStudent_id()==null&&personaResult.getProfesor_id()==null){
            personaRepository.deleteById(id);
            return personaResult;
        }
        return null;
    }


    private boolean camposOk(Persona persona) throws UnprocessableEntityException {
        boolean ok = true;
        if (persona.getUsuario().isEmpty()){
            ok=false;
            throw new UnprocessableEntityException("El usuario no puede ser nulo");
        }
        if(persona.getUsuario().length()>10||persona.getUsuario().length()<6){
            ok=false;
            throw new UnprocessableEntityException("Longitud del usuario no puede ser mayor a 10 ni menor a 6 caracteres");
        }
        if (persona.getPassword().isEmpty()){
            ok=false;
            throw new UnprocessableEntityException("La contraseña no puede ser nula");
        }
        if (persona.getName().isEmpty()){
            ok=false;
            throw new UnprocessableEntityException("El nombre no puede ser nulo");
        }
        if (persona.getCompany_email().isEmpty()){
            ok=false;
            throw new UnprocessableEntityException("El company_email no puede ser nulo");
        }
        if (persona.getPersonal_email().isEmpty()){
            ok=false;
            throw new UnprocessableEntityException("El personal_email no puede ser nulo");
        }
        if (persona.getCity().isEmpty()){
            ok=false;
            throw new UnprocessableEntityException("La ciudad no puede ser nula");
        }
        if (persona.getActive()==null){
            ok=false;
            throw new UnprocessableEntityException("El active no puede ser nulo");
        }
        if (persona.getCreated_date()==null){
            ok=false;
            throw new UnprocessableEntityException("La fecha no puede ser nula");
        }
        return ok;
    }
}
