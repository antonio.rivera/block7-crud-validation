package com.example.block7crudvalidation.service.impl;

import com.example.block7crudvalidation.model.Asignatura;
import com.example.block7crudvalidation.model.AsignaturaRepository;
import com.example.block7crudvalidation.model.Student;
import com.example.block7crudvalidation.model.StudentRepository;
import com.example.block7crudvalidation.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final AsignaturaRepository asignaturaRepository;
    @Override
    public Student crearStudent(Student student) {

        return studentRepository.save(student);
    }

    @Override
    public List<Student> mostrarStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Student mostrarPorId(Integer id_student) {
        return studentRepository.findById(id_student).orElse(null);
    }

    @Override
    public Student modificarStudent(Integer id_student, Student student) {
        Student studentModified = studentRepository.findById(id_student).orElse(null);
        if (studentRepository.findById(id_student).isPresent()){
            studentModified.setBranch(student.getBranch());
            studentModified.setNum_hours_week(student.getNum_hours_week());
            studentModified.setComents(student.getComents());
            studentRepository.save(studentModified);
        }
        return studentModified;
    }

    @Override
    public Student eliminarStudent(Integer id_student) {
        Student studentEliminado = studentRepository.findById(id_student).orElse(null);
        if (Objects.nonNull(studentEliminado)){
            studentRepository.deleteById(id_student);
        }
        return studentEliminado;
    }

    @Override
    public List<Asignatura> estudianteAsignaturas(Integer id) {

        return studentRepository.findById(id).get().getAsignaturas();
    }

    @Override
    public Student agregarAsignaturas(List<Integer> listaAsignaturas, Integer id) {
        Student student = studentRepository.findById(id).orElse(null);
        List<Asignatura> asignaturaList = new ArrayList<>();
        // Compruebo que las asignaturas existen
        for (Integer idAsignatura:listaAsignaturas){
            if(asignaturaRepository.findById(idAsignatura).isPresent()){
                asignaturaList.add(asignaturaRepository.findById(idAsignatura).orElse(null));
            }
        }
        // Compruebo que el student existe
        if(studentRepository.findById(id).isPresent()){
            student.setAsignaturas(asignaturaList);
            studentRepository.save(student);
        }
        return student;
    }

    @Override
    public Student eliminarAsignaturas(List<Integer> listaAsignaturas, Integer id) {
        int i=0;
        List<Asignatura> asignaturaList = studentRepository.findById(id).get().getAsignaturas();
        for (Integer idAsig:listaAsignaturas){
            if (asignaturaList.get(i).getId_asignatura()==idAsig){
                asignaturaList.remove(i);
            }
            i++;
        }
        studentRepository.findById(id).get().setAsignaturas(asignaturaList);
        studentRepository.save(studentRepository.findById(id).get());
        return studentRepository.findById(id).orElse(null);

    }
}
