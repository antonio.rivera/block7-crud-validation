package com.example.block7crudvalidation.service.impl;


import com.example.block7crudvalidation.model.Profesor;
import com.example.block7crudvalidation.model.ProfesorRepository;
import com.example.block7crudvalidation.service.ProfesorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class ProfesorServiceImpl implements ProfesorService {
    
    @Autowired
    private ProfesorRepository profesorRepository;
    
    @Override
    public Profesor crearProfesor(Profesor profesor) {
        return profesorRepository.save(profesor);
    }

    @Override
    public List<Profesor> mostrarProfesores() {
        return profesorRepository.findAll();
    }

    @Override
    public Profesor mostrarPorId(Integer id_profesor) {
        return profesorRepository.findById(id_profesor).orElse(null);
    }

    @Override
    public Profesor modificarProfesor(Integer id_profesor, Profesor profesor) {
        Profesor profesorModified = profesorRepository.findById(id_profesor).orElse(null);
        if (profesorRepository.findById(id_profesor).isPresent()){
            //profesorModified.setId_profesor(profesor.getId_profesor());
            profesorModified.setComents(profesor.getComents());
            profesorModified.setBranch(profesor.getBranch());
            //profesorModified.setPersona(profesor.getPersona());
            //profesorModified.setStudents(profesor.getStudents());
            profesorRepository.save(profesorModified);
        }
        return profesorModified;
    }

    @Override
    public Profesor eliminarProfesor(Integer id_profesor) {
        Profesor profesorEliminado = profesorRepository.findById(id_profesor).orElse(null);
        if (profesorRepository.findById(id_profesor).isPresent()){
            profesorRepository.deleteById(id_profesor);
        }
        return profesorEliminado;
    }
}
